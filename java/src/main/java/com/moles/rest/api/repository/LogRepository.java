package com.moles.rest.api.repository;

import com.moles.rest.api.entities.InfoLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogRepository extends CrudRepository<InfoLog, Integer> {
}
