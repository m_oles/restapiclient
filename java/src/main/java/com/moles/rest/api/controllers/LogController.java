package com.moles.rest.api.controllers;

import com.moles.rest.api.entities.InfoLog;
import com.moles.rest.api.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class LogController {

    @Autowired
    private LogRepository logRepository;

    @GetMapping("/logs")
    public List<InfoLog> logsAll() {
        List<InfoLog> result = new ArrayList<>();
        Iterable<InfoLog> all = logRepository.findAll();
        all.iterator().forEachRemaining(result::add);
        return result;
    }
}
