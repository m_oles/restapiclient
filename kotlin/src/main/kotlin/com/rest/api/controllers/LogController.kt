package com.rest.api.controllers

import com.rest.api.entities.InfoLog
import com.rest.api.repositories.LogRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class LogController(private val repository:LogRepository) {



    @GetMapping("/logs")
    fun logsAll(): List<InfoLog> {
        val findAll = repository.findAll()
        val toList = findAll.iterator().asSequence().toList()
        return toList
    }
}