package com.rest.api.entities

import javax.persistence.*

@Entity
@Table(name = "INFO_LOG")
data class InfoLog(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Int = -1,

        @Column(name = "INFO")
        var info: String = ""
) {}