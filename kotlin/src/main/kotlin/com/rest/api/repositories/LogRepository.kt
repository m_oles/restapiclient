package com.rest.api.repositories

import com.rest.api.entities.InfoLog
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface LogRepository: CrudRepository<InfoLog, Integer> {
}