from flask import Flask
import psycopg2
import json

hostname = 'localhost'
username = 'postgres'
password = 'postgres'
database = 'postgres'

SELECT="SELECT id, info FROM public.info_log;"

app = Flask(__name__)

def getLogs( conn ) :
    result=[]
    cur = conn.cursor()

    cur.execute( SELECT )

    for id,info in cur.fetchall() :
        el = {
            "id": id,
            "info": info,
        }
        result.append(el)
    return result


@app.route('/logs')
def logs():
    myConnection = psycopg2.connect( host=hostname, user=username, password=password, dbname=database )
    result=getLogs( myConnection )
    return json.dumps(result)

if __name__ == '__main__':
    app.run(port=8084)