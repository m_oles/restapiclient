package main

import (
	"fmt"
	"net/http"
	"log"
	"encoding/json"
	"database/sql"
	_ "github.com/lib/pq"
	"container/list"
)
const (
	hostname = "localhost"
	host_port = 5432
	username = "postgres"
	password = "postgres"
	database_name = "postgres"
)

var SELECT="SELECT id, info FROM public.info_log;"

type Info struct {
	ID   int    `json:"id"`
	INFO string `json:"info"`
}

func main() {
	fmt.Println("Serwer running in http://localhost:8083")

	http.HandleFunc("/logs",  getLogs)
	log.Fatal(http.ListenAndServe(":8083", nil))
}

func getLogs(w http.ResponseWriter, r *http.Request) {
	var con = createCon()
	data := getAll(con)
	n := data.Len()
	result := make([]Info, n)
	i := 0
	for temp := data.Front(); temp != nil; temp = temp.Next() {
		result[i] = temp.Value.(Info)
		i++
	}
	json.NewEncoder(w).Encode(result)
}

func createCon() *sql.DB {

	pg_con_string := fmt.Sprintf("port=%d host=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host_port, hostname, username, password, database_name)

	db, err := sql.Open("postgres", pg_con_string)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("db is connected")
	}
	err = db.Ping()
	if err != nil {
		fmt.Println(err)
		fmt.Println("Postgres db is not connected")
		fmt.Println(err.Error())
	}
	return db
}

func getAll(con *sql.DB) *list.List  {
	values := list.New()

	results, err := con.Query(SELECT)
	if err != nil {
		panic(err.Error())
	}
	for results.Next() {
		var info Info
		err = results.Scan(&info.ID, &info.INFO)
		if err != nil {
			panic(err.Error())
		}
		values.PushFront(info)
	}
	defer results.Close()

	return values
}
